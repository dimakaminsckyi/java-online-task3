package com.epam.exceptions;

import java.io.IOException;

public class CreateChannelException extends IOException {

    public CreateChannelException() {
    }

    public CreateChannelException(String message) {
        super(message);
    }
}
