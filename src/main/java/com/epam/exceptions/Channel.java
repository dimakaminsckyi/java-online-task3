package com.epam.exceptions;

public class Channel implements AutoCloseable {

    public void createChannel() throws CreateChannelException {
        throw new CreateChannelException("Can't create channel");
    }

    @Override
    public void close() throws ChannelCloseException {
        throw new ChannelCloseException("Can't close channel");
    }
}
