package com.epam.exceptions;

import java.io.IOException;

public class ChannelCloseException extends IOException {

    public ChannelCloseException() {
    }

    public ChannelCloseException(String message) {
        super(message);
    }
}
