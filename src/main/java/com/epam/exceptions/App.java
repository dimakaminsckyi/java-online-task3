package com.epam.exceptions;


public class App {

    public static void main(String[] args) {

        try (Channel channel = new Channel()) {
            channel.createChannel();
        } catch (ChannelCloseException | CreateChannelException cc) {
            cc.printStackTrace();
        }
    }
}
