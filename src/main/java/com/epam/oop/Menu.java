package com.epam.oop;

public enum Menu {
    a("Show all"),
    b("Range car"),
    c("Fuel Consumption"),
    d("Price of Taxi Park"),
    f("Add car to park"),
    q("Exit");

    private String meaning;

    Menu(String meaning) {
        this.meaning = meaning;
    }
}
