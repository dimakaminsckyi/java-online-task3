package com.epam.oop;

import com.epam.oop.manager.TaxiParkManager;
import com.epam.oop.model.CarType;

import java.util.Scanner;

public class App {

    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        TaxiParkManager park = new TaxiParkManager();
        createMenu(park);
    }

    private static void createMenu(TaxiParkManager park) {
        boolean flag = true;
        Menu menu;
        CarType carType;
        while (flag) {
            try {
                showMenu();
                menu = Menu.valueOf(scan.nextLine());
                switch (menu) {
                    case a:
                        park.getCar().forEach(System.out::println);
                        break;
                    case b:
                        System.out.println("Enter max speed car what you want : ");
                        park.calculateSpeedRange(scan.nextInt()).forEach(System.out::println);
                        break;
                    case c:
                        park.sortByFuelConsumption().forEach(System.out::println);
                        break;
                    case d:
                        System.out.println(park.countPrice());
                        break;
                    case f:
                        System.out.println("Enter type car :");
                        carType = CarType.valueOf(scan.nextLine());
                        park.createCar(carType, scan);
                        break;
                    case q:
                        scan.close();
                        flag = false;
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Enter correct option");
            }
        }
    }

    private static void showMenu() {
        System.out.println("Press option");
        System.out.println("a - Show all car in the taxi park");
        System.out.println("b - Range Car");
        System.out.println("c - Show cars by fuel consumption");
        System.out.println("d - Show price of taxi park");
        System.out.println("f - Add car to park");
        System.out.println("q - Exit");
    }
}
