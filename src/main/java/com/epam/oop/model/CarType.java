package com.epam.oop.model;

public enum CarType {
    Coupe,
    Crossover,
    Hatchback,
    Sedan
}
