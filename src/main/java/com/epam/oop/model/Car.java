package com.epam.oop.model;

import java.io.Serializable;

public abstract class Car implements Serializable {

    private String brand;
    private String model;
    private CarType carType;
    private int maxSpeed;
    private int fuelConsumption;
    private int price;

    public Car(String brand, String model, int maxSpeed, int fuelConsumption, int price) {
        this.brand = brand;
        this.model = model;
        this.carType = getCarType();
        this.maxSpeed = maxSpeed;
        this.fuelConsumption = fuelConsumption;
        this.price = price;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public int getPrice() {
        return price;
    }

    public abstract CarType getCarType();

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", carType=" + carType +
                ", maxSpeed=" + maxSpeed +
                ", fuelConsumption=" + fuelConsumption +
                ", price=" + price +
                '}';
    }
}
