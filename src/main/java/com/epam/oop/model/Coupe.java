package com.epam.oop.model;

public class Coupe extends Car {

    public Coupe(String brand, String model, int maxSpeed, int fuelConsumption, int price) {
        super(brand, model, maxSpeed, fuelConsumption, price);
    }

    @Override
    public CarType getCarType() {
        return CarType.Coupe;
    }
}
