package com.epam.oop.model;

public class Sedan extends Car {

    public Sedan(String brand, String model, int maxSpeed, int fuelConsumption, int price) {
        super(brand, model, maxSpeed, fuelConsumption, price);
    }

    @Override
    public CarType getCarType() {
        return CarType.Sedan;
    }
}
