package com.epam.oop.model;

public class Crossover extends Car {

    public Crossover(String brand, String model, int maxSpeed, int fuelConsumption, int price) {
        super(brand, model, maxSpeed, fuelConsumption, price);
    }

    @Override
    public CarType getCarType() {
        return CarType.Crossover;
    }
}
