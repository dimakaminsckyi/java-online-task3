package com.epam.oop.manager;

import com.epam.oop.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TaxiParkManager {

    private List<Car> cars = new ArrayList<>();
    private final String FILE_PATH =
            getClass().getClassLoader().getResource("vehicles").getFile();

    public int countPrice() {
        return cars.stream().mapToInt(Car::getPrice).sum();
    }

    public List<Car> calculateSpeedRange(int range) {
        return cars.stream().filter(o -> o.getMaxSpeed() >= range).collect(Collectors.toList());
    }

    public List<Car> sortByFuelConsumption() {
        return cars.stream().sorted(Comparator.comparingInt(Car::getFuelConsumption))
                .collect(Collectors.toList());
    }

    public List<Car> getCar() {
        getCarListFromFile();
        return cars;
    }

    public void createCar(CarType carType, Scanner scan) {
        System.out.println("Car brand : ");
        String brand = scan.nextLine();
        System.out.println("Car model : ");
        String model = scan.nextLine();
        System.out.println("Car max speed");
        int maxSpeed = scan.nextInt();
        System.out.println("Car fuel consumpption : ");
        int fuelConsumption = scan.nextInt();
        System.out.println("Vehcie price : ");
        int price = scan.nextInt();
        switch (carType) {
            case Sedan:
                cars.add(new Sedan(brand, model, maxSpeed, fuelConsumption, price));
                break;
            case Hatchback:
                cars.add(new Hatchback(brand, model, maxSpeed, fuelConsumption, price));
                break;
            case Crossover:
                cars.add(new Crossover(brand, model, maxSpeed, fuelConsumption, price));
                break;
            case Coupe:
                cars.add(new Coupe(brand, model, maxSpeed, fuelConsumption, price));
                break;
        }
        saveCarListToFile();
    }

    private void saveCarListToFile() {
        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_PATH);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(cars);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getCarListFromFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream(FILE_PATH);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            List<Car> carList = (List<Car>) objectInputStream.readObject();
            cars.clear();
            cars.addAll(carList);
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException ic) {
            ic.printStackTrace();
        }
    }
}
